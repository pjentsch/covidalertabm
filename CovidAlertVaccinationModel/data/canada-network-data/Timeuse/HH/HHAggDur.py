#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 2021-01-29

@author: mark

In the ABM model we wish to assign contact durations based on YMO brackets
The household composition data coming from HHComp.py has durations based on
the relationship between the respondent and the contact. 
In this file we extract YMO contact numbers and aggregate durations.
When adding times from different relationship compartments we assume that
the contacts overlap. We account for the size of this overlap by taking the
expected total coverage in the Intervals Model. 
"""

import numpy as np
import pandas as pd
import Intervals_Model as INT
import itertools
def Flatten(A):
    return list(itertools.chain.from_iterable(A))

HHComp = pd.read_csv("HHComp.csv")

# Keys corresponding to each age bracket
Ykeys =['U15CHILD', 'O15CHILD', 'YSPOUSE']
Mkeys = ['MCHILD', 'MPAR', 'MSPOUSE', 'MHHADULT']
Okeys = ['OPAR', 'OSPOUSE', 'OHHADULT']

# Define row functions for numbers Y, M and O contacts
def NumCont(row):    
    return pd.Series([np.sum(row[Ykeys]), np.sum(row[Mkeys]), np.sum(row[Okeys])])

# Define row functions for the aggregate contact durations
# Combined duration depends on the sigma value used. 
# Mean doesn't seem to matter so 60 is chosen
sigmalist=[6,12,18]
SIG = len(sigmalist)
def Duragg(row):
    # Attribute duration keys to Y
    Ydurkeys = ['U15CHILDDUR']
    if row['YSPOUSE']!= 0:
        Ydurkeys = Ydurkeys + ['SPOUSEDUR']
    if row['O15CHILD']!=0:
        Ydurkeys = Ydurkeys + ['O15CHILDDUR']
    # Attribute duration keys to M
    Mdurkeys = ['HHADULTDUR'] # Assigned this key to both M and O. Only 2 instances of both being non-zero
    if row['MPAR']!=0:
        Mdurkeys = Mdurkeys+['PARDUR']
    if row['MCHILD']!=0: #Thankfully MCHILD and O15CHILD are mutually exclusive
        Mdurkeys = Mdurkeys+['O15CHILDDUR']
    if row['MSPOUSE']!=0:
        Mdurkeys = Mdurkeys+['SPOUSEDUR']
    # Attribute duration keys to O
    Odurkeys = ['HHADULTDUR']
    if row['OPAR']!=0:
        Odurkeys = Odurkeys +['PARDUR']
    if row['OSPOUSE']!=0:
        Odurkeys = Odurkeys + ['SPOUSEDUR']
    # Corresponding list of durations
    Ydurlist = np.array(row[Ydurkeys], dtype='int_')
    Mdurlist = np.array(row[Mdurkeys], dtype='int_')
    Odurlist = np.array(row[Odurkeys], dtype='int_')
    # Combine durations using expected overlap of intervals
    Yout = [np.round(np.average(INT.TotDurSample(50, [60, sigmalist[i]], Ydurlist))) for i in range(SIG)]
    Mout = [np.round(np.average(INT.TotDurSample(50, [60, sigmalist[i]], Mdurlist))) for i in range(SIG)]
    Oout = [np.round(np.average(INT.TotDurSample(50, [60, sigmalist[i]], Odurlist))) for i in range(SIG)]
    return pd.Series(Yout+Mout+Oout)

# Combination of all columns to be added
def Combined(row):
    return NumCont(row).append(Duragg(row), ignore_index=True)

# Names of the columns to be added
Numlist = ['YNUM', 'MNUM', 'ONUM']
DurlistY = ['YDUR'+str(sigmalist[i]) for i in range(len(sigmalist))]
DurlistM = ['MDUR'+str(sigmalist[i]) for i in range(len(sigmalist))]
DurlistO = ['ODUR'+str(sigmalist[i]) for i in range(len(sigmalist))]
Durlist = DurlistY + DurlistM + DurlistO
Collist = Numlist + Durlist

# Add the columns and save the file
HHYMO = HHComp[['PUMFID', 'WGHT_PER', 'AGERESP']]
HHYMO[Collist] = HHComp.apply(Combined, axis=1)
HHYMO.to_csv("HHYMO.csv", index=False)


        
        
    
