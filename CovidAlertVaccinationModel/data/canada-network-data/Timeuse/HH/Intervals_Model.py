#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 2021-01-27

@author: mark

In this module we define the INTERVALS model for daily contact durations. 
Inputs: Number of contacts and duration of each contact

The starting points S_i drawn from NORMAL distribution on {0,...,144} with 
specified mu and sigma



The collection of intervals is then \cup_{i=1}^k (S_i, S_i + d_i). Note that the case S_i + d_i > 144 
requires special treatment. We will assume that the intervals are in fact on a circle.
"""
import numpy as np
from numpy import random
import matplotlib.pyplot as plt
import pandas as pd
import scipy.stats as st
from scipy import signal
from scipy import optimize
import math
import itertools


# Set the maximum duration
Durmax = 145

# Starting point. Uses NORMAL distribution, but this is flexible. 
# Truncate samples and return modulo 144
def StartSample(N, Sparam):
    mean, sigma = Sparam
    Starts = random.normal(mean,sigma, size = N).astype(int)%(Durmax-1)
    return Starts

# Samples interval configurations for a given sequence of durations
def IntSample(N, Sparam, durlist):
    numcontact = len(durlist)
    out = {}
    for i in range(N):
        S = StartSample(numcontact, Sparam)
        E = [(S[j] + durlist[j])%(Durmax-1) for j in range(numcontact)]

        out[i] = list(zip(S,E))
    return out


# A function that takes an interval and returns the set of timesteps contained in it
def Coverage(S,E):
    if S==E:
        return set()
    elif E < S:
        SUP = [i for i in range(S, Durmax-1,1)]
        EDOWN = [i for i in range(0, E)]
        return set(SUP + EDOWN)
    else:
        return set([i for i in range(S, E)])
        

# Samples the total contact duration for a given number of and durations per contact
def TotDurSample(N, Sparam, durlist):
    Ints = IntSample(N, Sparam, durlist)
    out = np.full(N,0)
    for i in range(N):
        Cov = set()
        for j in range(len(Ints[i])):
            S= Ints[i][j][0]
            E = Ints[i][j][1]
            Cov = Cov.union(Coverage(S,E))
        out[i] = len(Cov)
    return out




