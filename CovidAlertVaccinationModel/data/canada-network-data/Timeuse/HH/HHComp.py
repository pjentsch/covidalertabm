#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 2021-01-27

@author: mark

In this file we extract household composition and duration data from the 
Statcan timeuse survey

The duration keys don't require that the contact happened IN the
household. E.g., respondents record contact time with parents when they don't 
live with their parents. In such a case we manually set the duration to 0
"""
import numpy as np
from numpy import random
import pandas as pd

# The first step is to load the full dataset.
Maindf = pd.read_csv("../gss-89M0034-E-2015-c-29-main_F2.csv")


# Start by extracting the relevant columns from Maindf
HomeCols = ['PUMFID','WGHT_PER', 'AGEGR10', 'AGEPRGRD', 'PHSDFLG', 'MULTIGEN', 'HSDELIGC', 'HSDSIZEC', 'CHINHSDC', 'PARNUM', 
            'DURS201', 'DURS202', 'DURS203', 'DURS204', 'DURS205', 'LIVARR11', 'CHH0014C', 'CXR0014C']
Homedf = Maindf[HomeCols].copy()


# There are now a number of columns you want to add recording household contacts in different categories
# LIVARR11 consists of 11 keys which describe the respondent's household
# The first 10 rows we will add, which count number of HH contact types, are:
# - U15CHILD, O15CHILD, MCHILD, MPAR, OPAR, YSPOUSE, MSPOUSE, OSPOUSE, MHHADULT, OHHADULT
# To compute some of these entries we'll first define some special functions

# The first compute the age bracket of the respondent's spouse
def SPOUSEAGE(row):
    age = row['AGEGR10']
    Spouseage = row['AGEPRGRD']
    if age == 1:
        if Spouseage in [14,15]:
            return 'M'
        else:
            return 'Y'    
    elif age < 5:
        return 'M'
    elif age == 5:
        if Spouseage == 15:
            return 'O'
        else:
            return 'M'
    elif age ==6:
        if Spouseage in [1,2]:
            return 'M'
        else:
            return 'O'
    elif age == 7:
        if Spouseage == 1:
            return 'M'
        else:
            return 'O'
    else:
        return 'O'
# And then the function which computes the spousal contribution to the 10 entries
def SPOUSEADD(row):
    spousekey = row['AGEPRGRD']
    if spousekey == 96:
        return pd.Series(np.full(10,0))
    else:
        if SPOUSEAGE(row) == 'Y':
            return pd.Series([0,0,0,0,0,1,0,0,0,0])
        
        if SPOUSEAGE(row) == 'M':
            return pd.Series([0,0,0,0,0,0,1,0,0,0])
        else:
            return pd.Series([0,0,0,0,0,0,0,1,0,0])
        
# Determine Bracket (U15, O15) for young children, conditioned on knowing children are <25
# Note this is FAMILIAL children, not age 
def YCHILDADD(row):
    Tot = row['CHINHSDC']
    U = row['CXR0014C']
    O = max(Tot - U,0)
    return pd.Series([U,O,0,0,0,0,0,0,0,0])

# Parent contribution
def PARADD(row):
    parage = row['AGEGR10']+2 # Assumes parents are 2 age brackets higher
    parnum = row['PARNUM']
    if parage < 6:
        return pd.Series([0,0,0,parnum,0,0,0,0,0,0])
    else:
        return pd.Series([0,0,0,0,parnum,0,0,0,0,0])

# Non familial children >15 and other hh adults are both problematic because
# the survey gives no direct answer as to how many of them there are
# The only information is that the combined number of both is the same as
# the household size minus parents, nonfamilial children <15 and spouses.
# This residual must be split between O15Child and HHAdult
# We use the DURS keys. 
def CHILDADD(row):
    U = row['CHH0014C']
    spousenum = (1+(-1)**(row['PHSDFLG']+1))/2 # Key is 1 = Yes, 2 = No
    Res = max(row['HSDSIZEC'] -1 - row['PARNUM'] - row['CHH0014C'] - spousenum,0)
    if Res == 0:
        return pd.Series([U,0,0,0,0,0,0,0,0,0])
    else:
        O15key = row['DURS203']
        ADkey = row['DURS205']
        if ADkey + O15key ==0:
            ratio = 1/2
        else:
            ratio = O15key/(ADkey + O15key)
        O = np.round(Res*ratio)
        return pd.Series([U,O,0,0,0,0,0,0,0,0])
    
# Other household adults contribution.
# I explicitly assume they are M UNLESS the respondent lives in a multigenerational household and is age 1 or 2
# In the latter case, a coin is flipped as to whether there are 1 or 2 O adults
def HHADULTADD(row): 
    spousenum = (1+(-1)**(row['PHSDFLG']+1))/2 # Key is 1 = Yes, 2 = No
    if row['LIVARR11'] in [3,4,5,6]: # Respondent lives with familial children
        HHADNUM = max(row['HSDSIZEC']-1  - row['PARNUM'] - row['CHINHSDC'] - spousenum, 0)
    else: # No familial children. Must split with O15child
        Res = max(row['HSDSIZEC']-1  - row['PARNUM'] - row['CHH0014C'] - spousenum, 0)
        if Res == 0:
            HHADNUM = 0
        else:
            O15key = row['DURS203']
            ADkey = row['DURS205']
            if ADkey + O15key ==0:
                ratio = 1/2
            else:
                ratio = O15key/(ADkey + O15key)
            HHADNUM = Res - np.round(Res*ratio)
    multi = row['MULTIGEN']
    age = row['AGEGR10']
    if multi == 1 and age in [1,2]:
        if HHADNUM == 0:
            NumO = 0
        elif HHADNUM > 1:
            NumO = random.randint(1,3)
        elif HHADNUM==1:
            NumO = 1
        return pd.Series([0,0,0,0,0,0,0,0,HHADNUM - NumO, NumO])
    else:
        return pd.Series([0,0,0,0,0,0,0,0,HHADNUM, 0])
        
    
    
# The labels of the new columns
Counts = ['U15CHILD', 'O15CHILD', 'MCHILD', 'MPAR', 'OPAR', 'YSPOUSE',
          'MSPOUSE', 'OSPOUSE', 'MHHADULT', 'OHHADULT']

# The function creating the new columns
def COUNTS(row):
    HHcomp = row['LIVARR11']
    if HHcomp == 1: # Alone
        return pd.Series([0,0,0,0,0,0,0,0,0,0])
    elif HHcomp == 2: # Spouse
        return SPOUSEADD(row) + HHADULTADD(row)+PARADD(row)
    elif HHcomp == 3: # Spouse and children < 25
        return SPOUSEADD(row) + YCHILDADD(row) + HHADULTADD(row)+PARADD(row)
    elif HHcomp ==4: # Spouse and children > 25
        return SPOUSEADD(row) + pd.Series([0,0,row['CHINHSDC'],0,0,0,0,0,0,0]) + HHADULTADD(row) + PARADD(row)
    elif HHcomp == 5: # Spouse and Other
        return SPOUSEADD(row) + HHADULTADD(row) + PARADD(row)
    elif HHcomp == 6: # No spouse, children < 25
        return YCHILDADD(row) + HHADULTADD(row) + PARADD(row)
    elif HHcomp == 7: # No spouse, children > 25
        return pd.Series([0,0,row['CHINHSDC'],0,0,0,0,0,0,0])
    elif HHcomp == 8: # Two parents
        return PARADD(row)+CHILDADD(row) + HHADULTADD(row)
    elif HHcomp == 9: # one parent
        return PARADD(row) +CHILDADD(row) + HHADULTADD(row)
    elif HHcomp == 10: # No spouse and other
        return (HHADULTADD(row) + PARADD(row) + CHILDADD(row))
    elif HHcomp == 11: # Unspecificied
        return (SPOUSEADD(row)+HHADULTADD(row) + PARADD(row) + CHILDADD(row))    
    
# Age of respondent
def AGERESP(row):
    age = row['AGEGR10']
    if age == 1:
        return 'Y'
    elif age < 6:
        return 'M'
    else:
        return 'O'
    
def ContRows(row):
    out = pd.Series(AGERESP(row)).append([COUNTS(row)], ignore_index=True)
    return out

Contlist = ['AGERESP']+Counts
Homedf[Contlist] = Homedf.apply(ContRows, axis=1) 


# Change duration values to units of 10 minutes
# The survey reports contacts which must have occurred outside the household.
# This is evidenced by the respondent indicated no member of that class in
# their household, yet has non-zero duration with that class
# We correct this by setting these durations to 0
def DURS(row):
    durkeys = ['DURS20'+str(i) for i in range(1,6)]
    durs = row[durkeys].to_numpy()
    rounded = [np.around(durs[i]/10) for i in range(len(durs))]
    spousenum = int(row['YSPOUSE'])+int(row['MSPOUSE']) + int(row['OSPOUSE'])
    O15num = int(row['O15CHILD']) + int(row['MCHILD'])
    Parnum = int(row['MPAR'])+int(row['OPAR'])
    HHAdnum = int(row['MHHADULT'])+int(row['OHHADULT'])
    if spousenum == 0 and rounded[0]!=0: # No spouse
        rounded[0] = 0
    if int(row['U15CHILD'])==0 and rounded[1] != 0: #No U15
        rounded[1] = 0
    if O15num ==0 and rounded[2] != 0: # No O15 
        rounded[2] = 0
    if Parnum ==0 and rounded[3]!= 0:
        rounded[3] = 0
    if HHAdnum == 0 and rounded[4]!=0:
        rounded[4]==0    
    return pd.Series(rounded)


Durlist = ['SPOUSEDUR', 'U15CHILDDUR', 'O15CHILDDUR', 'PARDUR', 'HHADULTDUR']
Homedf[Durlist] = Homedf.apply(DURS, axis=1)

# Save the home composition file
COLsave = ['PUMFID','WGHT_PER']+Contlist + Durlist
Homedf[COLsave].to_csv("HHComp.csv", index=False)