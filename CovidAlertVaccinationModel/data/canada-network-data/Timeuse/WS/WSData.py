#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 2021-02-08

@author: mark

Short script to organize the workschool duration data from the timeuse survey.
Aggregates reported durations according to age bracket of respondent
"""
import numpy as np
import pandas as pd
import itertools
def Flatten(A):
    return list(itertools.chain.from_iterable(A))

#  load the full dataset.
Maindf = pd.read_csv("../gss-89M0034-E-2015-c-29-main_F2.csv")

#create the workschool dataframe
Workschooldf = Maindf[['PUMFID','WGHT_PER', 'AGEGR10', 'DURS208']].copy()
# Add age bracket
def BRACADD(row):
    age = row['AGEGR10']
    if age ==1 :
        return 'Y'
    elif age < 6:
        return 'M'
    else:
        return 'O'
Workschooldf['BRAC'] = Workschooldf.apply(BRACADD, axis=1)

# Record duration in units of 10 minutes
def DURSADD(row):
    durs = np.round(row['DURS208']/10)
    return durs
Workschooldf['DURS'] = Workschooldf.apply(DURSADD, axis=1)

# Then assemble the data
Brac = ('Y', 'M', 'O')
WorkschoolData = {}
for x in Brac:
    DF = Workschooldf
    WorkschoolData[x] = DF[DF['BRAC']==x][['PUMFID','WGHT_PER', 'DURS']]
 
# Save to csv    
for x in Brac:
    Datadf = pd.DataFrame(WorkschoolData[x])
    Datadf.to_csv('WorkschoolData'+x+'.csv', index=False)
