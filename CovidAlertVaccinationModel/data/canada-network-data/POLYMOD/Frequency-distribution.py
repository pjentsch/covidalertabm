#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 2021-03-24

Determines the mixing matrices for locales_nohh for each frequency bin:
    - 'daily' = key 1
    - '3xweekly' = key 2
    - 'justonce' = keys 3-5



@author: mark
"""
import numpy as np
import pandas as pd
import itertools
from itertools import product
import csv

# Read the cleaned survey as a dataframe
Contact = pd.read_csv("AALContact_data.csv")


# Specify AAL categories
Ages = ['Y', 'M', 'O']
Locales = ['home', 'workschool', 'rest']

# Dictionary separating contact data by symage-location
ContAAL = {}
columns = ['part_id', 'cont_id', 'age_source', 'age_target', 
           'duration_multi', 'frequency_multi']
for x,y,z in list(product(Ages,Ages,Locales)):
    C = Contact[Contact['cnt_'+z] == True][Contact['age_source']==x][Contact['age_target']==y]
    ContAAL[(x,y,z)] = C[columns].copy(deep=True)
    
# Determine frequency distributions accord to AAL, excluding hh
freqdistAAL={}
locales_nohh = ['workschool', 'rest']
for x,y, z in list(product(Ages, Ages, locales_nohh)):
    C = ContAAL[(x,y,z)]
    tot = len(C)
    freqfull = np.full(5,0.)
    for i in range(1,6):
        count = len(C[C['frequency_multi']==i])
        freqfull[i-1] = (count)/tot
    freqdistAAL[(x,y,z)] = [freqfull[0], freqfull[1], np.sum(freqfull[2:])]
    

# Import Prem mixing matrices by locales_nohh
Prem = {}
with open('../Prem/wsMixing.csv', newline='') as csvfile:
    Prem['workschool'] = np.array(list(csv.reader(csvfile))[1:], dtype='f')
with open('../Prem/restMixing.csv', newline='') as csvfile:
    Prem['rest'] = np.array(list(csv.reader(csvfile))[1:], dtype='f')

# Initialize mixing matrices and reindexers for age and freq bins
Mixing={}
freqbins = ['daily', '3xweekly', 'justonce']
for loc, fbins in list(product(locales_nohh, freqbins)):
    Mixing[(loc, fbins)] = np.full((3,3), 0.) 
age_swap={'Y':0, 'M':1, 'O': 2}
freq_swap={'daily':0, '3xweekly':1, 'justonce':2}

# Subdivide mixing matrices by localces_nohh and frequency
for x,y, z, fbins in list(product(Ages, Ages, locales_nohh, freqbins)):
    i = age_swap[x]
    j = age_swap[y]
    rescale = freqdistAAL[(x,y,z)][freq_swap[fbins]]
    Mixing[(z, fbins)][i][j] = rescale*Prem[z][i][j]

# Function which flattens an array to a list
def Flatten(A):
    return list(itertools.chain.from_iterable(A))

# Flatten the mixing subdivided mixing matrices
flat_mixing = {}
for loc, fbins in list(product(locales_nohh, freqbins)):
    print(Mixing[(loc,fbins)])
    flat_mixing[(loc,fbins)] = Flatten(Mixing[(loc,fbins)])
    
# Save them to csv
df = pd.DataFrame(list(flat_mixing.items()), columns=['col1','col2'])
dfkeys = pd.DataFrame([pd.Series(x) for x in df.col1])
dfkeys.columns = ["location", "frequency"]
dfvals = pd.DataFrame([pd.Series(x) for x in df.col2])
dfout = dfkeys.join(dfvals)
dfout.to_csv("mixing_loc-freq.csv")