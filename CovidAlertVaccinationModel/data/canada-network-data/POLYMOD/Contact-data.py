#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 2021-03-23

Here we tidy and organize the POLYMOD data and save it to a csv

@author: mark
"""
import numpy as np
import pandas as pd
import math
from itertools import product

# To start, let's load the two files: The contact and participant data
Contact = pd.read_csv("2008_Mossong_POLYMOD_contact_common.csv")
Part = pd.read_csv("2008_Mossong_POLYMOD_participant_common.csv")

# Delete contacts missing age, duration and frequency data
Contact = Contact.dropna(subset=['cnt_age_exact', 'duration_multi', 'frequency_multi'])

# Delete contacts missing location data
def Loctest(row):
    emptycheck = row['cnt_home'] or row['cnt_work'] or row['cnt_school'] or row['cnt_transport'] or row['cnt_leisure'] or row['cnt_otherplace']
    if emptycheck == False:
        return 1
    else:
        return 0
Contact['Loctest'] = Contact.apply(Loctest, axis=1)
Contact = Contact[Contact['Loctest']==0].copy(deep=True)


# Next, we aggregate the locations
# Define the function computing WorkSchool value from remaining entries
def WorkSchool(row):
    return row['cnt_work'] or row['cnt_school']
# Create the new column
Contact['cnt_workschool'] = Contact.apply(WorkSchool, axis=1)

# Define the function computed Rest value from remaining entries
def Rest(row):
    return row['cnt_transport'] or row['cnt_leisure'] or row['cnt_otherplace']
# And then create the new column
Contact['cnt_rest'] = Contact.apply(Rest, axis=1)

# We also add a column with the source age bracket. First step is to define age bracket value
def Agesource(row):
    ID = row['part_id']
    year = float(Part[Part['part_id'] ==ID]['part_age'])
    if year < 25:
        return 'Y'
    elif year < 65:
        return 'M'
    else:
        return 'O'
# Then create the new source column
Contact['age_source'] = Contact.apply(Agesource, axis=1)

# Columns function for target age bracket
def Agetarget(row):
    ID = row['cont_id']
    year = float(Contact[Contact['cont_id'] ==ID]['cnt_age_exact'])
    if year < 25:
        return 'Y'
    elif year < 65:
        return 'M'
    else:
        return 'O'
# Add the new target column
Contact['age_target'] = Contact.apply(Agetarget, axis=1)

#save a copy of the new contact dataframe
Tosave = ['part_id', 'cont_id', 'age_source', 'age_target', 'cnt_home', 
          'cnt_workschool', 'cnt_rest', 'duration_multi', 'frequency_multi']
Contact[Tosave].to_csv("AALContact_data.csv", index=False)