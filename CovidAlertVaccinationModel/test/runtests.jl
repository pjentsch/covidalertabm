using OnlineStats: neighbors
using CovidAlertVaccinationModel
using RandomNumbers.Xorshifts
using Test
using ThreadsX
using BenchmarkTools
import StatsBase.mean

include("ABM/mixing_test.jl")
include("ABM/output_test.jl")
include("IntervalsModel/intervals_model_test.jl")