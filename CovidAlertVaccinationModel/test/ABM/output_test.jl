using CovidAlertVaccinationModel:vaccination_data,ymo_vac,ymo_attack_rate
using OnlineStats
using Plots
const samples = 50
using Random

@testset "default parameters output" begin
    p =  CovidAlertVaccinationModel.get_parameters()
    out,avg_populations = mean_solve(samples, p)
    # plot_model(nothing,[nothing],[out],p.infection_introduction_day,p.immunization_begin_day)

    # ymo_vaccination_ts = mean.(out.daily_immunized_by_age)

    total_postinf_vaccination = mean.(out.total_postinf_vaccination)#sum.(eachrow(ymo_vaccination_ts[:,180:end]))
    final_size = mean.(out.unvac_final_size_by_age)#sum.(eachrow(mean.(out.daily_unvac_cases_by_age)))
    total_preinf_vaccination = mean.(out.total_preinf_vaccination)#sum.(eachrow(ymo_vaccination_ts[:,1:180]))
    target_final_size = ymo_attack_rate .*avg_populations
    target_preinf_vac = ymo_vac .* sum(vaccination_data[1:4]) .* avg_populations
    target_postinf_vac = ymo_vac .* sum(vaccination_data[5:end]) .*avg_populations

  
    println("obs final size: $final_size, target: $target_final_size")
    println("obs preinf vac: $total_preinf_vaccination, target: $target_preinf_vac")
    println("obs postinf vac: $total_postinf_vaccination,target: $target_postinf_vac")

    @test all(abs.(final_size .- target_final_size) .< [75,100,25])
    @test all(abs.(total_preinf_vaccination .- target_preinf_vac) .< [50,100,25])
    @test all(abs.(total_postinf_vaccination .- target_postinf_vac) .< [135,150,80])
end

@testset "perf" begin
    b = @belapsed CovidAlertVaccinationModel.bench()
    @test  3.0 < b < 4.5
    if Threads.nthreads() == 25
        b = @belapsed CovidAlertVaccinationModel.threaded_bench()
        @test  12.0 < b < 16.00
    else
        display("incorrect number of threads, skipping threaded perf test")
    end
end
