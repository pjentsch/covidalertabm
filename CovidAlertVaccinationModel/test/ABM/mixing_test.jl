
using CovidAlertVaccinationModel:abm,generate_contact_vectors!
using CovidAlertVaccinationModel:ModelSolution,AgentDemographic,mean,get_weight
using CovidAlertVaccinationModel:AgentStatus,get_u_0,get_parameters,solve!, WeightedGraph, contact_time_distributions,GraphEdge
using LightGraphs
using Random
using Plots
using UnPack
using ThreadsX
using OnlineStats
using StatsBase
const model_sizes = [1000,5000]
 const dem_cat = AgentDemographic.size -1
 const samples = 1


@testset "mixing matrices, size: $sz" for sz in model_sizes
    for rep = 1:samples
        m = ModelSolution(100,get_parameters(),sz)
        ws_dist = m.ws_matrix_tuple
        r_dist = m.rest_matrix_tuple 
        index_vec =m.index_vectors
        @testset "workschool" for i in dem_cat, j in dem_cat
            for t in 1:length(ws_dist)
                @test mean(ws_dist[t][i,j])*length(index_vec[i]) == mean(ws_dist[t][j,i])*length(index_vec[j])
            end
        end
        @testset "rest" for i in dem_cat, j in dem_cat
            for t in 1:length(ws_dist)
                @test mean(r_dist[t][i,j])*length(index_vec[i]) == mean(r_dist[t][j,i])*length(index_vec[j])
            end
        end
    end
end

function approximate_mixing_matricies_weights_dict(p,sol)
    mean_mixing_degree = zeros(3,3)
    mean_mixing_weighted_degree = zeros(3,3)
        for g_list in sol.inf_network.graph_list
            for (k,g) in enumerate(g_list)
                for e in keys(g.weights_dict)
                    demo_i = sol.demographics[e.a]
                    demo_j = sol.demographics[e.b]
                    mean_mixing_degree[Int(demo_i), Int(demo_j)] += 1
                    mean_mixing_degree[Int(demo_j), Int(demo_i)] += 1
                    mean_mixing_weighted_degree[Int(demo_i), Int(demo_j)] += get_weight(g,GraphEdge(e.a,e.b))
                    mean_mixing_weighted_degree[Int(demo_j), Int(demo_i)] += get_weight(g,GraphEdge(e.a,e.b))
                end 
        end
    end
    mean_mixing_degree ./= (p.sim_length .* length.(sol.index_vectors) * 2)
    mean_mixing_weighted_degree ./= (p.sim_length .* length.(sol.index_vectors) * 2)
    return mean_mixing_degree, mean_mixing_weighted_degree
end

function approximate_mixing_matricies_graph(p,sol)
    mean_mixing_degree = zeros(3,3)
    mean_mixing_weighted_degree = zeros(3,3)
        for g_list in sol.inf_network.graph_list
            for g in g_list
                for e in edges(g.g)  
                        demo_i = sol.demographics[src(e)]
                        demo_j = sol.demographics[dst(e)]
                        mean_mixing_degree[Int(demo_i), Int(demo_j)] += 1#get_weight(g,GraphEdge(node_i,node_j)) /2
                        mean_mixing_degree[Int(demo_j), Int(demo_i)] += 1#get_weight(g,GraphEdge(node_i,node_j)) /2
                        mean_mixing_weighted_degree[Int(demo_i), Int(demo_j)] += get_weight(g,GraphEdge(src(e),dst(e)))
                        mean_mixing_weighted_degree[Int(demo_j), Int(demo_i)] += get_weight(g,GraphEdge(src(e),dst(e)))
                end 
            end
        end
        mean_mixing_degree ./= (p.sim_length .* length.(sol.index_vectors) * 2)
        mean_mixing_weighted_degree ./= (p.sim_length .* length.(sol.index_vectors) * 2)
    return mean_mixing_degree, mean_mixing_weighted_degree
end
@testset "weights dict and graph match" begin
    p = get_parameters()
    sol = abm(p)
    graph_deg_matrix,graph_wdeg_matrix = approximate_mixing_matricies_graph(p,sol)
    dict_deg_matrix,dict_wdeg_matrix = approximate_mixing_matricies_weights_dict(p,sol)

    @test all(graph_deg_matrix .≈ dict_deg_matrix)
    @test all(graph_wdeg_matrix .≈ dict_wdeg_matrix)
end

using OnlineStats


@testset "contact vector generation" begin
    model = ModelSolution(1, get_parameters(), 1_000_000) #big
    @unpack demographics, index_vectors, rest_matrix_tuple,ws_matrix_tuple = model
    
    for dist in (ws_matrix_tuple...,rest_matrix_tuple...)
        for i in 1:3, j in 1:i  #diagonal
            if i != j 
                num_degrees_ij = similar(index_vectors[i])
                num_degrees_ji = similar(index_vectors[j])
                generate_contact_vectors!(dist[i,j],dist[j,i],num_degrees_ij,num_degrees_ji) 
                @test sum(num_degrees_ji) == sum(num_degrees_ij)
                @test mean(num_degrees_ij) ≈ mean(dist[i,j]) atol = 1e-2
                @test mean(num_degrees_ji) ≈ mean(dist[j,i]) atol = 1e-2
                @test var(num_degrees_ij) ≈ var(dist[i,j]) atol = 1e-1
                @test var(num_degrees_ji) ≈ var(dist[j,i]) atol = 1e-1
                @test skewness(num_degrees_ij) ≈ skewness(dist[i,j]) atol = 5e-1
                @test skewness(num_degrees_ji) ≈ skewness(dist[j,i]) atol = 5e-1
            else
                num_degrees_ii = similar(index_vectors[i])
                generate_contact_vectors!(dist[i,j],num_degrees_ii) 
                @test iseven(sum(num_degrees_ii))
                @test mean(num_degrees_ii) ≈ mean(dist[i,j]) atol = 1e-2
                @test var(num_degrees_ii) ≈ var(dist[i,j]) atol = 1e-1
                @test skewness(num_degrees_ii) ≈ skewness(dist[i,j]) atol = 5e-1
            end
        end
    end
end

@testset "degree distribution generation"  for _ in samples
    model = ModelSolution(1, get_parameters(), 100_000)

    dist_means = (
        [3.1955821797791937 0.9429428771683844 0.0031430699141405485; 0.7199525059785551 2.4721641553924263 0.0044567453216716795; 0.009353408942771433 0.017370616608004246 0.00426932603311176],
        [0.8019318380935724 0.651677764149297 0.0018836516558612103; 0.4975667675635904 1.316746659531406 0.0026840811411943774; 0.00560552731065053 0.010461478294876817 0.006192152261765137],    
        [0.1128548274049412 0.3230353075170861 0.003964429647800997; 0.2466428081676342 1.258148402093756 0.004582169674064005; 0.011797679572415589 0.017859470733932858 0.01238430452353015], 
        [1.0193293323987982 0.3561306290520396 0.030138426493779822; 0.27191163436292753 0.5144739702660603 0.08850778467147245; 0.08968843697040778 0.3449680615304364 0.10943814365793207],   
        [0.906277378657788 0.37875635184860545 0.048350709654810434; 0.28918674849909 0.842274696054987 0.1455507801394707; 0.14388606439838253 0.5672989180028651 0.2322383000912529], 
        [0.8128613982828048 0.7034672332223655 0.06881899421762766; 0.5371088850611215 1.8960399351138069 0.250096158670163; 0.20479728848911483 0.9747751271020686 0.32411028549080995],
    )   


    @unpack demographics, index_vectors, rest_matrix_tuple, ws_matrix_tuple = model
    for (expected_dist_mean,dist) in zip(dist_means,(ws_matrix_tuple...,rest_matrix_tuple...))
        g = WeightedGraph(demographics,index_vectors,dist,contact_time_distributions.ws)
        mixing_dist = [Variance() for _ in 1:3, _ in 1:3]
        mixing_weights = [Variance() for _ in 1:3, _ in 1:3]

        for v in vertices(g.g)  
            demo_v = demographics[v]
            degs = zeros(3)
            for w in LightGraphs.neighbors(g.g,v)
                demo_w = demographics[w]
                degs[Int(demo_w)] += 1
                fit!(mixing_weights[Int(demo_v),Int(demo_w)],get_weight(g,GraphEdge(w,v)))
            end
            for (j,d) in enumerate(degs)
                fit!(mixing_dist[Int(demo_v), j],d)
            end
        end
        # display(mean.(mixing_dist))
        # display(expected_dist_mean)
        for i in eachindex(mixing_dist)
            @test mean(mixing_dist[i]) ≈ expected_dist_mean[i]  atol = 0.05
            @test mean(mixing_dist[i]) ≈ mean(dist[i])  atol = 0.2
        end

        for i in 1:3, j in 1:i
            @test mean(mixing_weights[i,j]) ≈ contact_time_distributions.ws[i,j].μ atol = 0.4
        end
    end
end

# @testset "edge resampling" begin
#     model = abm(get_parameters(), nothing)
#     @unpack demographics, inf_network, soc_network = model
#     for l in inf_network.resampled_graphs
#         for graphs in inf_network.graph_list
#             mixing_dist = [0.0 for _ in 1:3, _ in 1:3]
#             prev_mixing_dist = [0.0 for _ in 1:3, _ in 1:3]
#             if l in graphs
#                 for v in vertices(l.g)  
#                     demo_v = demographics[v]
#                     degs = zeros(3)
#                     for w in LightGraphs.neighbors(l.g,v)
#                         demo_w = demographics[w]
#                         degs[Int(demo_w)] += get_weight(l,GraphEdge(w,v))
#                     end
#                     for (j,d) in enumerate(degs)
#                         mixing_dist[Int(demo_v), j] += d
#                     end
#                 end
#             end
#             prev_mixing_dist = copy(mixing_dist)
#             display((prev_mixing_dist,mixing_dist))
#         end
#     end
# end
