"""
    get_params(params::Vector{Number})


This function reorganizes a Vector of 6n parameters into a 3x3 symmetric matrix of n-tuples of parameters.
"""
function get_params(params)
    p_list = [as_symmetric_matrix(params[i:i+5]) for i = 1:6:length(params)]
    return zip(p_list...) |> collect
end


"""
Turn a vector of length 6, l, into a symmetric 3x3 matrix, probably a nicer way to do this exists but meh.
"""
function as_symmetric_matrix(l) 
    return [
        l[1] l[2] l[3]
        l[2] l[4] l[5]
        l[3] l[5] l[6]
    ]
end

"""
    Turn a symmetric 3x3 matrix, l, into a vector of length 6, probably a nicer way to do this exists but meh.
"""
function symmetric_matrix_as_vector(A) 
    return [A[1,1],A[1,2],A[1,3],A[2,2],A[2,3],A[3,3]]
end

function from_mean(::Type{Geometric{T}},μ) where T
    return Geometric(1/(μ+1))
end
function from_mean(::Type{Poisson{T}},μ) where T
    return Poisson(μ)
end
function from_mean_workschool(_,μ)
    return Poisson(μ)
end


function display_weighted_degree(g)
    weighted_degree = 0.0
    for node in vertices(g.g) 
        for j in neighbors(g,node)
            weighted_degree += get_weight(g,GraphEdge(node,j))
        end
    end
    display(weighted_degree)
end
function display_degree(g)
    weighted_degree = 0.0
    for node in vertices(g.g) 
        for j in neighbors(g,node)
            weighted_degree += 1
        end
    end
    display(weighted_degree)
end