module CovidAlertVaccinationModel
using LightGraphs
using RandomNumbers.Xorshifts
using Random
using UnPack
using Plots
using Distributions
using StatsBase
using OnlineStats
using Dates
using LinearAlgebra
using CovidAlertVaccinationModel
using ThreadsX
using DelimitedFiles
using KernelDensity
using NamedTupleTools
using NetworkLayout:Stress
using NetworkLayout:SFDP
using ZeroWeightedDistributions
using DataStructures
using Serialization
using BenchmarkTools
using Intervals
using KissABC
using CSV
using DataFrames
using StaticArrays
import LightGraphs.neighbors

export intervalsmodel, hh, ws, rest, abm,multivariate_simulations

# const DNDEBUG = false
# macro c_assert(boolean) #this is a version of @assert that turns itself off when DNDEBUG=false, should use more
#     if DNDEBUG
#          message = string("Assertion: ", boolean, " failed")
#          :($(esc(boolean)) || error($message))
#     end
# end

const durmax = 144
const PACKAGE_FOLDER = dirname(dirname(pathof(CovidAlertVaccinationModel)))

const color_palette = palette(:seaborn_pastel) #color theme for the plots

include("utils.jl")
include("data.jl")
include("ABM/contact_vectors.jl")
include("ABM/mixing_distributions.jl")
include("ABM/mixing_graphs.jl")
include("ABM/agents.jl")
include("ABM/model_setup.jl")
include("ABM/parameter_optimization.jl")
include("ABM/output.jl")
include("ABM/solve.jl")
include("ABM/abm.jl") 
include("ABM/parameter_planes.jl")
include("ABM/plotting.jl")

include("IntervalsModel/intervals_model.jl")
include("IntervalsModel/interval_overlap_sampling.jl")
include("IntervalsModel/hh_durations_model.jl")
include("IntervalsModel/ws_durations_model.jl")
include("IntervalsModel/rest_durations_model.jl")
include("IntervalsModel/plotting_functions.jl")

end
