"""
Number of start times to sample for a given set of distribution parameters. 
"""
const comparison_samples = 100
const distribution_support = 0:94
const YOUNG, MIDDLE,OLD = 1,2,3
const sample_repeat = 100
const sparam = (60,12)

# Swap age brackets for numbers
const swap_dict = OrderedDict("Y" => YOUNG, "M" => MIDDLE, "O" => OLD)
"""
Runs parameter estimation for the three scenarios, hh, ws, and rest. 
"""
function intervalsmodel()
    community(1000)
    # hh(1000)
    # ws(1000)
    # rest(1000)
    plot_all()
end

"""
    bayesian_estimation(
        fname::String, 
        err_func::Function, 
        priors_list::Vector{Distribution}, 
        dists::Vector{Distribution}, 
        particles::Integer;
        alpha = 0.98
    )

# Arguments

- fname 

Filename to save output particles. Will be saved in `simulations/` as `fname.dat`. The format is just straight serialized Julia objects (using Serialization.jl), which are neither small nor fast to load, but very convient.

- err_func(p,distribution)

A function that takes two arguments, a vector of particles with 6n elements for some integer n, and a distribution to fit that takes n parameters. 

- priors_list

A list of 6n distributions, for some integer n, that describe the prior distribution of parameters for a distribution with n parameters. 

- dists

A list of univariate distributions to fit to the distribution of contact times.

- particles

Number of particles to obtain representing posterior distribution of the distribution parameters.

# Optional Arguments

- alpha = 0.98

Threshold adaptive rate, see `KissABC.smc` for more details.
"""
function bayesian_estimation(fname, err_func, priors_list, dists, particles; alpha = 0.98)
    
    data_pairs = map(zip(dists,priors_list)) do (dist,priors)
        init = rand(priors)
        # @btime $err_func($init,$dist)
        out = smc(priors,p -> err_func(p, dist), verbose=true, nparticles=particles, parallel = true)
        return string(dist) => out
    end |> OrderedDict

    serialize(joinpath(PACKAGE_FOLDER,"intervals_model_output/simulation_output","$fname.dat"),data_pairs)
end

"""
    hh(particles::Integer)

Fit HH durations. This function specifies a list of distributions to fit, and loads the priors for "hh", and then calls `bayesian_estimation` with `err_hh`. This function passes parameters to `bayesian_estimation` depending on what produces the best fit for the HH data.
"""
function hh(particles)
    dists = [
        Poisson,
    ]

    poisson_priors = filter_priors("home")
    # display(poisson_priors)
    # Set parameter priors for fitting
    priors_list = [
        Factored(poisson_priors...),
    ]
    bayesian_estimation("hh",err_hh,priors_list,dists,particles; alpha = 0.98)
end

"""
    hh(particles::Integer)

Fit HH durations. This function specifies a list of distributions to fit, and loads the priors for "hh", and then calls `bayesian_estimation` with `err_hh`. This function passes parameters to `bayesian_estimation` depending on what produces the best fit for the HH data.
"""
function community(particles)
    dists = [
        Poisson,
    ]

    poisson_priors = filter_priors("workschool")
    # display(poisson_priors)
    # Set parameter priors for fitting
    priors_list = [
        Factored(poisson_priors...),
    ]
    community_error(p,d) = err_ws(p,d) + err_rest(p,d)
    bayesian_estimation("community",community_error,priors_list,dists,particles; alpha = 0.98)
end

"""

    ws(particles::Integer)

Fit WS durations. This function specifies a list of distributions to fit, and loads the priors for "workschool", and then calls `bayesian_estimation` with `err_ws`. This function passes parameters to `bayesian_estimation` depending on what produces the best fit for the WS data.
"""
function ws(particles)
    dists = [
        Poisson,
    ]
    poisson_priors = filter_priors("workschool")
    # display(poisson_priors)
    # Set parameter priors for fitting
    priors_list = [
        Factored(poisson_priors...)
    ]
    bayesian_estimation("ws",err_ws,priors_list,dists,particles; alpha = 0.98)
end

"""

    rest(particles::Integer)

Fit rest durations. This function specifies a list of distributions to fit, and loads the priors for "rest", and then calls `bayesian_estimation` with `err_rest`. This function passes parameters to `bayesian_estimation` depending on what produces the best fit for the Rest data.
"""
function rest(particles)
    dists = [
        Poisson,
    ]
    poisson_priors = filter_priors("rest")
    # Set parameter priors for fitting
    priors_list = [
        Factored(poisson_priors...)
    ]
    bayesian_estimation("rest",err_rest, priors_list, dists, particles, alpha = 0.98)
end

