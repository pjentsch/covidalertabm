
const initial_workschool_mixing_matrix = make_workschool_mixing_matrix() 
const ws_data = get_ws_data()
"""
Kernel density functions based on the durs in ws_data and their corresponding weights. This defines an interpolated density function that we can compare to the density obtained from the simulation.
"""

const kerneldensity_data_ws = (
    Y = InterpKDE(kde(ws_data.Y.durs;weights = ws_data.Y.weights)),
    M = InterpKDE(kde(ws_data.M.durs;weights = ws_data.M.weights)),
    O = InterpKDE(kde(ws_data.O.durs;weights = ws_data.O.weights)),
)
const data_ws = hcat(
    map(i->pdf(kerneldensity_data_ws.Y,i), 0:143),
    map(i->pdf(kerneldensity_data_ws.M,i), 0:143),
    map(i->pdf(kerneldensity_data_ws.O,i), 0:143),
)
"""
    err_ws(p::Vector{Number},dist::Distribution)

Calculate error between observed contact durations in workschool data and simulated contact durations based on `dist(p[i]...)`. For these workschool simulations, we use a different sampling procedure for neighbourhood sizes based on `ws_sample` in CovidAlertVaccinationModel.
"""
function err_ws(p,dist)
    #we get the vector p as a 9 element vector from KissABC, but more convenient as a 3x3 matrix

    p_matrix = get_params(p)

    #create a matrix of distributions of type dist, from the parameters in p_matrix
    age_dists = [dist(p_matrix[i,j]...) for i in YOUNG:OLD, j in YOUNG:OLD] 

    #initialize the list of samples and the total error
    sample_list = zeros(comparison_samples)
    errsum = 0
    for _ in 1:sample_repeat
        for age_sample in YOUNG:OLD
            #Sample neighbourhood sizes. Note that this includes the weekend/weekday coinflip, implemented in CovidAlertVaccinationModel.  
            neighourhoods = ws_sample(age_sample)

            for age_j in YOUNG:OLD #for a given age_sample loop over possible contact ages        
                if neighourhoods[age_j] > 0
                    #get durations from our candidate distributions for each of the contacts in neighbourhood
                    durs = trunc.(Int,rand(Random.default_rng(Threads.threadid()),age_dists[age_sample,age_j],neighourhoods[age_j])) .% durmax

                    #this MODIFIES sample_list to contain samples from the distribution of total_durations, given intervals of length dur.
                    tot_dur_sample!(sample_list,durs)

                    #compute a kernel density from the list of samples
                    kde_est = kde(sample_list)
                    interpolant = InterpKDE(kde_est) #so we don't need to reinterpolate the kernel each time 

                    #compute L1 distance between observed distribution of total durations from survey, and observed distribution from simulation
                    errsum += mapreduce(+,0:143) do i
                        return abs(pdf(interpolant,i) - data_ws[i+1,age_sample]) #arrays indexed from 1, so need +1 in the second term
                    end
                end
            end
        end
    end
    return errsum./sample_repeat
end

