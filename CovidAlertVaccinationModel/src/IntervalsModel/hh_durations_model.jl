
const HHYMO = DataFrame(CSV.File("$PACKAGE_FOLDER/data/canada-network-data/Timeuse/HH/HHYMO.csv"))
const Wghttotal = sum(HHYMO[:,"WGHT_PER"])

function make_dat_array()
    durs = hcat(
        Int.(HHYMO[!,"YDUR"*string(sparam[2])]),
        Int.(HHYMO[!,"MDUR"*string(sparam[2])]),
        Int.(HHYMO[!,"ODUR"*string(sparam[2])]),
    )
    nums = hcat(
        Int.(HHYMO[!,"YNUM"]),
        Int.(HHYMO[!,"MNUM"]),
        Int.(HHYMO[!,"ONUM"]),
    )
    WGHT = Weights(HHYMO[!,"WGHT_PER"]./Wghttotal)
    AGERESP = map(r -> swap_dict[r],HHYMO[!,"AGERESP"])
    return (;
        nums,
        durs,
        WGHT,
        AGERESP
    )
end
const dat = make_dat_array() #assign a constant data array

"""

    err_hh(p,dist)

Calculate error between observed total contact durations in HH data and simulated total contact durations based on `dist(p[i]...)`. This one works a bit differently than the WS or rest error function, since we do not have a distribution of total durations to compare to, but rather one average total duration, and we do have the explicit samples of numbers of contacts.
"""
function err_hh(p,dist)
    p_matrix = get_params(p)
    age_dists = [dist(p_matrix[i,j]...) for i in YOUNG:OLD, j in YOUNG:OLD]
    duration_subarray =  dat.durs #durations, these are call subarray because we used to only sample a subset
    num_contacts_subarray = dat.nums #numbers of contacts for each duration
    AGERESP =  dat.AGERESP #age of the respondent
    errsum = 0
    @inbounds for i = 1:length(AGERESP) #loop through entire file
        age_sample = AGERESP[i]
        @inbounds for age_j in YOUNG:OLD #for a given age_sample loop over possible contact ages

            #get num_contacts from the HH data
            durs = trunc.(Int,rand(Random.default_rng(Threads.threadid()),age_dists[age_sample,age_j],num_contacts_subarray[i,age_j])) .% durmax

            #this returns the MEAN of the distribution of total durations, given we have durations given by durs, and we sample comparison_samples from that distribution.
            expdur = tot_dur_sample(comparison_samples,durs)
            errsum += (expdur - duration_subarray[i,age_j])^2 #compute total 
        end
    end
    return errsum
end