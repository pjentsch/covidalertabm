const rest_data = get_rest_data()
const kerneldensity_data_rest = (
    Y = InterpKDE(kde(rest_data.Y.durs,weights = rest_data.Y.weights)),
    M = InterpKDE(kde(rest_data.M.durs,weights = rest_data.M.weights)),
    O = InterpKDE(kde(rest_data.O.durs,weights = rest_data.O.weights)),
)
const data_rest = hcat(
    map(i->pdf(kerneldensity_data_rest.Y,i), 0:143),
    map(i->pdf(kerneldensity_data_rest.M,i), 0:143),
    map(i->pdf(kerneldensity_data_rest.O,i), 0:143),
)
const rest_distributions = symmetrize_means(get_household_data_proportions(),map(t->from_mean(t...),[
    (Geometric{Float64},2.728177)  (Geometric{Float64},1.382557)        (Geometric{Float64},0.206362) 
    (Geometric{Float64},1.139072)    (Geometric{Float64},3.245594)        (Geometric{Float64},0.785297)   
    (Geometric{Float64},0.264822)    (Geometric{Float64},0.734856)          (Geometric{Float64},0.667099) 
]))


"""
    err_rest(p::Vector{Number},dist::Distribution)

Calculate error between observed contact durations in rest data and simulated contact durations based on `dist(p[i]...)`.
"""
function err_rest(p,dist)
    #we get the vector p as a 6n element vector from KissABC, but more convenient as a 3x3 matrix of tuples
    p_matrix = get_params(p)

    
    #create a matrix of distributions of type dist, from the parameters in p_matrix
    age_dists = [dist(p_matrix[i,j]...) for i in YOUNG:OLD, j in YOUNG:OLD]
    
    #initialize the list of samples and the total error
    sample_list = zeros(comparison_samples)
    errsum = 0

    @inbounds for _ in 1:sample_repeat
        #sample a matrix of neighourhood sizes for distributions in rest_distributions
        neighourhoods = rand.(Random.default_rng(Threads.threadid()),rest_distributions)
        @inbounds for age_sample in YOUNG:OLD, age_j in YOUNG:OLD  #for a given age_sample loop over possible contact ages
                if neighourhoods[age_sample,age_j] > 0
                    #get durations from our candidate distribtions for each of the contacts in neighbourhood
                    durs = trunc.(Int,rand(Random.default_rng(Threads.threadid()),age_dists[age_sample,age_j],neighourhoods[age_sample,age_j])) .% durmax
                    
                    #this MODIFIES sample_list to contain samples from the distribution of total_durations, given intervals of length dur.
                    tot_dur_sample!(sample_list,durs)
                    
                    #compute a kernel density from the list of samples
                    kde_est = kde(sample_list)
                    interpolant = InterpKDE(kde_est) #so we don't need to reinterpolate the kernel each time 

                    #compute L1 distance between observed distribution of total durations from survey, and observed distribution from simulation
                    errsum += mapreduce(+,0:143) do i
                        return abs(pdf(interpolant,i) - data_rest[i+1,age_sample]) #arrays indexed from 1, so need +1 in the second term
                    end
                end
        end
    end
    return errsum./sample_repeat
end