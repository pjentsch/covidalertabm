const workschool_mixing, rest_mixing = load_mixing_matrices()
const age_bins = [(0.0, 25.0),(25.0,65.0),(65.0,Inf)]  
const household_data = read_household_data()
default(dpi = 300)
default(framestyle = :box)
import LightGraphs.neighbors
export DebugRecorder,mean_solve,plot_model, univariate_simulations, plot_parameter_plane, get_parameters, HeatmapRecorder
"""
    bench()

Runs the model with default parameters.
"""
function bench()
    p = get_parameters()
    out = abm(p)
    return out
end


"""
Runs the model once for each thread, using all threads.
"""
function threaded_bench()
    ThreadsX.map(t -> bench(),1:20)
end
using Distributed
function dist_bench()
    pmap(t -> bench(),1:nprocs())
end

"""
Run the model with given parameter tuple and output recorder. See `get_parameters` for list of parameters. See `output.jl` for the list of recorders. Currently just 'DebugRecorder`.
"""
function abm(parameters; record_degrees = false)
    model_sol = ModelSolution(parameters.sim_length,parameters,parameters.num_households;record_degrees)
    solve!(model_sol)
    return model_sol
end
