function contact_weight(β, contact_time) 
    return 1 - (1-β)^contact_time
end

function Φ(payoff,ξ)
    return 1 / (1 + exp(-1*ξ*payoff))
end



@views function update_alert_durations!(t,agent,modelsol) 
    @unpack notification_parameter,notification_threshold = modelsol.params
    @unpack time_of_last_alert,inf_network,covid_alert_notifications,is_app_user, output_data = modelsol

    for j in 2:14
        covid_alert_notifications[j-1,agent] = covid_alert_notifications[j,agent] #shift them all back  
    end
    total_weight_i = 0
    for mixing_graph in inf_network.graph_list[t], neighbor in neighbors(mixing_graph,agent)
        if is_app_user[neighbor]
            total_weight_i+= get_weight(mixing_graph,GraphEdge(agent,neighbor))
        end
    end
    coin_flip = 1 - (1 - notification_parameter)^total_weight_i
    r = rand(Random.default_rng(Threads.threadid()))
    if r < coin_flip
        covid_alert_notifications[end,agent] = 1  #add the notifications for today
    else
        covid_alert_notifications[end,agent] = 0
    end
    if sum(covid_alert_notifications[:,agent])>=notification_threshold
        output_data.daily_total_notifications[t] += 1
        time_of_last_alert[agent] = t
    end
end
function agent_transition!(t,modelsol,agent, from::AgentStatus,to::AgentStatus) 
    @unpack u_next_inf,output_data, immunization_countdown = modelsol
    immunization_countdown[agent] = -1   
    output_data.recorded_status_totals[Int(from), t+1] -= 1
    output_data.recorded_status_totals[Int(to), t+1] += 1
    u_next_inf[agent] = to
end
function is_susceptible_infected!(t, modelsol, agent,agent_inf_status,agent_demo,α_vec,β_vec)
    @unpack u_inf,inf_network, output_data = modelsol

    for mixing_graph in inf_network.graph_list[t], neighbor in neighbors(mixing_graph,agent)    
        if u_inf[neighbor] == Infected
            infection_threshold = contact_weight(β_vec[Int(agent_demo)],get_weight(mixing_graph,GraphEdge(agent,neighbor)))

            if rand(Random.default_rng(Threads.threadid())) < infection_threshold && agent_inf_status == Susceptible
                output_data.daily_cases_by_age[Int(agent_demo),t]+=1
                output_data.daily_unvac_cases_by_age[Int(agent_demo),t]+=1
                agent_transition!(t,modelsol, agent, Susceptible,Infected)
                return true
            end
        end
    end
    return false
end
function is_immunized_infected!(t, modelsol, agent,agent_inf_status,agent_demo,α_vec,β_vec)
    @unpack u_inf,inf_network, output_data = modelsol
    for mixing_graph in inf_network.graph_list[t], neighbor in neighbors(mixing_graph,agent)    
        if u_inf[neighbor] == Infected
            infection_threshold = contact_weight(β_vec[Int(agent_demo)],get_weight(mixing_graph,GraphEdge(agent,neighbor)))
            if rand(Random.default_rng(Threads.threadid())) < infection_threshold && agent_inf_status == Immunized
                immunization_ineffective = rand(Random.default_rng(Threads.threadid())) < 1- α_vec[Int(agent_demo)]
                if immunization_ineffective
                    agent_transition!(t,modelsol,agent, Immunized,Infected)
                    output_data.daily_cases_by_age[Int(agent_demo),t]+=1
                    return true
                end
            end
        end
    end
    return false
end
function vaccinate_agent!(t,agent,immunization_countdown)
    if immunization_countdown[agent] == -1 #not currently in countdown
        immunization_countdown[agent] = 14
    end
end

function update_vaccination_opinion_state!(t,agent,agent_demo,modelsol,vaccinate_today_flag,total_infections,π_base_vec)
    @unpack infection_introduction_day,immunization_intervals, η,Γ,ζ, ω, ω_en,ξ = modelsol.params
    @unpack immunization_countdown,demographics,time_of_last_alert, soc_network,u_vac,u_next_vac,is_app_user,output_data = modelsol
    random_soc_network = sample(Random.default_rng(Threads.threadid()), soc_network.graph_list[t])
    if !isempty(neighbors(random_soc_network,agent))
        random_neighbour = sample(Random.default_rng(Threads.threadid()), neighbors(random_soc_network.g,agent))
        app_vac_payoff = 0.0    
        if is_app_user[agent] && time_of_last_alert[agent]>=0
            output_data.daily_total_notified_agents[t] += 1
            app_vac_payoff = Γ^((t - time_of_last_alert[agent])) * (η + total_infections*ω_en)

        end
        output_data.total_app_weight[t] += app_vac_payoff
        if u_vac[random_neighbour] == u_vac[agent]
            vac_payoff = π_base_vec[Int(demographics[agent])] + total_infections*ω + app_vac_payoff
            if u_vac[agent]
                if rand(Random.default_rng(Threads.threadid())) < 1 - Φ(vac_payoff,ξ)
                    u_next_vac[agent] = false
                end
            else
                if rand(Random.default_rng(Threads.threadid())) < Φ(vac_payoff,ξ)
                    u_next_vac[agent] = true

                    if vaccinate_today_flag && t>infection_introduction_day
                        vaccinate_agent!(t, agent, immunization_countdown)
                        # fit!(output_data.avg_weighted_degree_of_vaccinators[Int(agent_demo)],weighted_degree_of_i)
                        if !is_app_user[agent]
                            # fit!(output_data.avg_weighted_degree_of_vaccinators_no_EN[Int(agent_demo)],weighted_degree_of_i)
                        end
                    end
                end
            end
            return vac_payoff

        end
    end
    return 0
end
using WeightedOnlineStats
function assortativity(t,network::TimeDepMixingGraph,u_inf)
    
    total_weight = 0.0
    x_stats = WeightedVariance()
    y_stats = WeightedVariance()

    for g in network.graph_list[t]
        for e in edges(g.g)
            v = src(e)
            w = dst(e)
            weight = get_weight(g,GraphEdge(v,w))
            total_weight += weight
            fit!(x_stats,(u_inf[v] == Immunized),weight)
            fit!(y_stats,(u_inf[v] != Immunized),weight)
        end
    end
    σ_x = std(x_stats)
    σ_y = std(y_stats)
    x_bar = mean(x_stats)
    y_bar = mean(y_stats)

    numerator = 0.0
    join_count_11 = 0.0
    join_count_01 = 0.0
    join_count_00 = 0.0
    for g in network.graph_list[t]
        for e in edges(g.g)
            v = src(e)
            w = dst(e)
            weight = get_weight(g,GraphEdge(v,w))
            v_inf = u_inf[v] == Immunized
            w_inf = u_inf[w] != Immunized
            numerator += weight*(v_inf - x_bar) * (w_inf - y_bar)
            #ughhhh
            join_count_11 += ifelse( u_inf[v] == Immunized &&  u_inf[w] == Immunized,weight,0)
            join_count_01 += ifelse(xor( u_inf[v] == Immunized, u_inf[w] == Immunized),weight,0)
            join_count_00 += ifelse(!(u_inf[v] == Immunized) && !(u_inf[w] == Immunized),weight,0)
        end
    end
    return numerator/(total_weight*σ_x*σ_y),(join_count_11,join_count_01,join_count_00)
end

function sample_initial_nodes(nodes,graphs,I_0_fraction)
    weighted_degrees = zeros(nodes)
    for v in 1:nodes
        for g in graphs
            for w in neighbors(g,v)
                weighted_degrees[v] += get_weight(g,GraphEdge(v,w))
            end
        end
    end
    wv = Weights(weighted_degrees ./sum(weighted_degrees))
    num = round(Int,nodes*I_0_fraction)
    init_indices = sample(Random.default_rng(Threads.threadid()), 1:nodes,wv, num; replace = false)
    return init_indices
end    


function agents_step!(t,modelsol)
    @unpack EN_intervals,immunizing,infection_introduction_day,recovery_rate,β_y,β_m,β_o,α_y,α_m,α_o,π_base_y,π_base_m,π_base_o,ζ,immunization_intervals = modelsol.params
    @unpack u_inf,u_vac,u_next_vac,u_next_inf,demographics,inf_network,soc_network, is_app_user, immunization_countdown, output_data = modelsol

    vaccinate_today_flag = 
        !immunizing ? false :
        (any(t in I for I in immunization_intervals) ? true :
        false)
        
    EN_today_flag = t in EN_intervals ? true : false

    u_next_inf .= u_inf
    u_next_vac .= u_vac
    modelsol.output_data.recorded_status_totals[:,t+1] .= modelsol.output_data.recorded_status_totals[:,t]
    total_infected = modelsol.output_data.recorded_status_totals[Int(Infected), t]
    β_vec = @SVector [β_y,β_m,β_o]
    α_vec = @SVector [α_y,α_m,α_o]
    if t<last(minimum(immunization_intervals))
        π_base_vec = @SVector [π_base_y,π_base_m,π_base_o]
    else
        π_base_vec = @SVector [π_base_y*ζ,π_base_m*ζ,π_base_o*ζ]
    end
    if t == last(minimum(immunization_intervals)) + 14
        weighted_assortativity, join_counts = assortativity(t,soc_network,u_inf)
        fit!(modelsol.output_data.assortativity_at_end,weighted_assortativity)
        fit!(modelsol.output_data.vac_join_11_counts_at_end,join_counts[1])
        fit!(modelsol.output_data.vac_join_01_counts_at_end,join_counts[2])
        fit!(modelsol.output_data.vac_join_00_counts_at_end,join_counts[3])
    end        
    for (agent,(agent_inf_status,agent_is_vaccinator,agent_demo)) in enumerate(zip(u_inf,u_vac,demographics))       
        if agent_inf_status == Susceptible 
            is_susceptible_infected!(t, modelsol, agent,agent_inf_status,agent_demo,α_vec,β_vec)
            if vaccinate_today_flag && agent_is_vaccinator && t<(infection_introduction_day-1)
                # fit!(output_data.avg_weighted_degree_of_vaccinators[Int(agent_demo)],weighted_degree_of_i)
                if !is_app_user[agent]
                    # fit!(output_data.avg_weighted_degree_of_vaccinators_no_EN[Int(agent_demo)],weighted_degree_of_i)
                end
                vaccinate_agent!(t, agent, immunization_countdown)
            end

        elseif agent_inf_status == Immunized
            is_immunized_infected!(t, modelsol, agent,agent_inf_status,agent_demo,α_vec,β_vec)
        elseif agent_inf_status == Infected
            if rand(Random.default_rng(Threads.threadid())) < recovery_rate
                agent_transition!(t,modelsol, agent, Infected,Recovered)
            end
        end
        if EN_today_flag && is_app_user[agent]
            update_alert_durations!(t, agent,modelsol)
        end
        if immunization_countdown[agent] == 0 
            output_data.daily_immunized_by_age[Int(agent_demo),t] += 1
            agent_transition!(t,modelsol, agent, Susceptible,Immunized)
        elseif immunization_countdown[agent]>0
            immunization_countdown[agent] -= 1
        end
        if agent_is_vaccinator
            # fit!(output_data.avg_weighted_degree_of_vaccinator_belief[t],weighted_degree_of_i)
        end
        update_vaccination_opinion_state!(t, agent,agent_demo, modelsol,vaccinate_today_flag, total_infected,π_base_vec)
    end
    output_data.total_vaccinators[t] = count(==(true),u_vac) 
    u_vac .= u_next_vac
    u_inf .= u_next_inf
    # return total_weight

end


function solve!(modelsol)
    init_indices = sample_initial_nodes(modelsol.nodes, modelsol.inf_network.graph_list[begin], modelsol.params.I_0_fraction)
    # total_weight = 0.0
    
    for t in 1:modelsol.sim_length  
        #this also resamples the soc network weights since they point to the same objects, but those are never used

        if t>1
            remake_all!(t,modelsol.inf_network,modelsol.index_vectors,modelsol.demographics)
        end

        if t>modelsol.params.infection_introduction_day
            if !isempty(init_indices)
                inf_index = pop!(init_indices)
                modelsol.u_inf[inf_index] = Infected
                modelsol.output_data.recorded_status_totals[Int(Infected),t] += 1
            end
           
        end


        agents_step!(t,modelsol)
        #advance agent states based on the new network
        
        record!(t,modelsol)
        # display(mean.(modelsol.output_data.avg_weighted_degree))

    end
    # display(total_weight)
end