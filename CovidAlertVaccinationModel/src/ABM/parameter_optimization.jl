using KissABC

const vaccination_data = [0.0,0.043,0.385,0.424,0.115,0.03,0.005] #by month starting in august
const ymo_vac = [0.255,0.278,0.602]
const ymo_attack_rate = [10.376,5.636,7.2]./100


function solve_w_parameters(default_p, p_names, new_p_list)

    new_params = merge(default_p, NamedTuple{p_names}(ntuple(i -> new_p_list[i],length(p_names))))
    
    out = DebugRecorder(0,default_p.sim_length)
    model = abm(new_params,out)
    return out, model
end

function fit_distribution_parameters(p_tuple)
    p_names = (:ω,:β_y,:β_m,:β_o,:π_base_y,:π_base_m,:π_base_o,:ζ)        
    
    β_y_val = 0.00077
    β_m_val = 0.00065
    β_o_val = 0.75
    priors = Factored(
        Uniform(0.0,0.01),
        TriangularDist(β_y_val-0.0005,β_y_val+0.0005),
        TriangularDist(β_m_val-0.0005,β_m_val+0.0005),
        TriangularDist(β_o_val-0.0005,β_o_val+0.0005), 
        Uniform(-5.0,3.0),
        Uniform(-5.0,3.0),
        Uniform(-5.0,3.0), 
        Uniform(1.0,2.0),   
    )
    #simulation begins in july
    #60 days for opinion dynamics to stabilize, then immunization begins in september,
    #infection introduced at beginning of december
    function cost(p)
        output,model = solve_w_parameters(p_tuple, p_names,p)
        ymo_vaccination_ts = output.daily_immunized_by_age

        total_postinf_vaccination = sum.(eachrow(ymo_vaccination_ts[:,181:end]))
        final_size = sum.(eachrow(output.daily_unvac_cases_by_age))
        total_preinf_vaccination = sum.(eachrow(ymo_vaccination_ts[:,1:180]))

        target_final_size = ymo_attack_rate .* length.(model.index_vectors)
        target_preinf_vac = ymo_vac .* sum(vaccination_data[1:4]) .* length.(model.index_vectors)
        target_postinf_vac = ymo_vac .* sum(vaccination_data[5:end]) .* length.(model.index_vectors)
    
        # display((final_size,target_final_size))
        # display((total_preinf_vaccination,target_preinf_vac))
        # display((total_postinf_vaccination,target_postinf_vac))
        # display(sum.(eachrow(ymo_vaccination_ts)) ./length.(model.index_vectors))

        return sum((total_preinf_vaccination .- target_preinf_vac).^2)  
        + sum((total_postinf_vaccination .- total_postinf_vaccination).^2)  
        + sum((final_size .- target_final_size).^2)
    end 
    #    display(cost((0.005,0.00075, 0.00063,0.75,-1.4,-1.4,-0.95,1.25)))
    
    out = smc(priors,cost; verbose = true, nparticles = 1000, parallel = true)
    return NamedTuple{p_names}(ntuple(i -> out.P[i].particles,length(p_names)))
end
function fit_parameters(default_parameters)
    # pre_inf_behaviour_parameters_path =joinpath(PACKAGE_FOLDER,"abm_parameter_fits","pre_inf_behaviour_parameters.dat")
    # post_inf_behaviour_parameters_path = joinpath(PACKAGE_FOLDER,"abm_parameter_fits","post_inf_behaviour_parameters.dat")
    fit_all_parameters_path = joinpath(PACKAGE_FOLDER,"abm_parameter_fits","fit_all_parameters.dat")
    # output = fit_distribution_parameters(default_parameters)
    # serialize(fit_all_parameters_path,output)

    
    fitted_parameter_tuple = deserialize(fit_all_parameters_path)
    fitted_sol,avg_populations = plot_fitting_posteriors("post_inf_fitting",fitted_parameter_tuple,default_parameters)

    ymo_vaccination_ts = mean.(fitted_sol.daily_immunized_by_age)

    total_postinf_vaccination = sum.(eachrow(ymo_vaccination_ts[:,180:end]))
    final_size = sum.(eachrow(mean.(fitted_sol.daily_unvac_cases_by_age)))
    total_preinf_vaccination = sum.(eachrow(ymo_vaccination_ts[:,1:180]))
    target_final_size = ymo_attack_rate .*avg_populations
    target_preinf_vac = ymo_vac .* sum(vaccination_data[1:4]) .* avg_populations
    target_postinf_vac = ymo_vac .* sum(vaccination_data[5:end]) .*avg_populations

  
    println("obs final size: $final_size, target: $target_final_size")
    println("obs preinf vac: $total_preinf_vaccination, target: $target_preinf_vac")
    println("obs postinf vac: $total_postinf_vaccination,target: $target_postinf_vac")

    total_final_size = sum.(eachrow(mean.(fitted_sol.daily_cases_by_age))) 
    println("vac + unvac cases proportion: $(total_final_size./avg_populations))")
    display(sum.(eachrow(ymo_vaccination_ts)) ./avg_populations) 

    return fitted_sol
end 

function plot_fitting_posteriors(fname,particles_tuple,parameters)
    stat_recorder = DebugRecorder(Variance(), parameters.sim_length)
    output_recorder = DebugRecorder(parameters.sim_length)
    avg_populations = [0.0,0.0,0.0]
    names = keys(particles_tuple)
    samples = collect(zip(particles_tuple...))
    for p_set in sample(samples,50)
        p_set_named = NamedTuple{names}(p_set)
        display(p_set_named)
        sol = abm(merge(parameters,p_set_named),output_recorder)
        avg_populations .+= length.(sol.index_vectors)
        fit!(stat_recorder,output_recorder)
    end
    avg_populations ./= 50
    p = plot_model(nothing,[nothing],[stat_recorder],parameters.infection_introduction_day,parameters.immunization_begin_day)
    savefig(p, "$fname.pdf")
    
    plts = [plot() for i in 1:length(particles_tuple)]
    for (plt,(k,v)) in zip(plts,pairs(particles_tuple))
        hist = StatsBase.fit(Histogram,v; nbins = 50)
        plot!(plt,hist;legend = false,xlabel = k)
    end
    p = plot(plts...; size = (1400,800),bottommargin = 5Plots.mm)
    savefig(p,"$(fname)_posteriors.pdf")
    return stat_recorder,avg_populations
end
