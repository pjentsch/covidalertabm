
const samples = 25
const univariate_path = "CovidAlertVaccinationModel/plots/univariate/" 
EN_begin_settings(p) = (
    all_season = Intervals.Interval(0,p.sim_length),
    pre_season = Intervals.Interval(0,p.infection_introduction_day),
    in_season = Intervals.Interval(p.infection_introduction_day-5, p.sim_length)
)

function univarate_test(parameters,variable, variable_range; progmeter = nothing)
    parameter_range_list = [merge(parameters,NamedTuple{(variable,)}((value,))) for value in variable_range]
    solve_fn(p) = mean_solve(samples, p;progmeter, record_degrees=true)[1]

    univariate_outlist = ThreadsX.map(solve_fn, parameter_range_list)
    
    p = plot_model(variable,parameter_range_list,univariate_outlist,parameters)
    return p
end

if !ispath(univariate_path)
    mkdir(univariate_path)
end
function univariate_simulations()
    len = 6
    params = get_app_parameters()
    univarate_test_list = (
        (:η, range(0.0,5.0; length = len)),
        (:ω_en, range(0.0, 1e-2; length = len)),
        (:notification_parameter, range(0.000, 0.001; length = len)),
        (:app_user_fraction, range(0.05, 0.8; length = len)),
        (:notification_threshold, (1:len)),
        (:EN_intervals, EN_begin_settings(params)),
        # (:Γ, range(1/7,1.0; length = len))
    )


    numsim = sum(map(t -> length(t[2]), univarate_test_list))
    display(numsim)
    progmeter = Progress(numsim*samples)
    display(params)
    plt_list = ThreadsX.map(univarate_test_list) do ur
        out = univarate_test(params,ur...;progmeter)
        display(out)
        return out
    end

    for ((varname,_),pltlist) in zip(univarate_test_list,plt_list)
        mkpath("$univariate_path/$varname")
        # display(pltlist)
        for (i,p) in enumerate(pltlist)
            savefig(p,"$univariate_path/$varname/$i.pdf")
        end
    end
end

import AxisKeys 
function multivariate_simulations()
    len = 10

    default_parameters = get_app_parameters() 
    app_simulations = (
        (:η,vcat([-0.5,-0.25],range(0.0, 3.0; length = len))),
        (:ω_en,vcat([-0.1,-0.05], range(0.0, 3e-1; length = len))),
        (:EN_intervals, EN_begin_settings(default_parameters))
        # (:notification_threshold, (1:len)),
    )
    run_multivariate_sims(app_simulations,default_parameters)


    # for ((varname,_),p) in zip(univarate_test_list,plt_list)
    #     savefig(p,"$univariate_path/$varname.pdf")
    # end
end
using ProgressMeter
function run_multivariate_sims(sims,default_parameters)
    varnames, sim_ranges = zip(sims...)

    simvars = Iterators.product(sim_ranges...)
    progmeter = Progress((length(simvars)+1)*samples)

    
    without_app, _ = mean_solve(samples,get_parameters(); progmeter, record_degrees = true)
    next!(progmeter)
    app_output = ThreadsX.map(simvars) do vars
        vars_with_names = NamedTuple{varnames}(vars)
        parameters = merge(default_parameters,vars_with_names)
        out,_ = mean_solve(samples, parameters;progmeter, record_degrees = true) 
        return out
    end
    display(length(simvars))
    fname = join(string.(varnames),"_")
    output_keys = map(sim_ranges) do sim_range
        if typeof(sim_range) <: NamedTuple
            return collect(keys(sim_range))
        end
        return sim_range
    end

    keyed_output = AxisKeys.KeyedArray(app_output;NamedTuple{varnames}(output_keys)...)
    path = joinpath(PACKAGE_FOLDER,"abm_output","$fname.dat")
    serialize(path,(without_app,keyed_output))
    return fname
end

using ColorSchemes
using LaTeXStrings

function plot_parameter_plane(input_fname)
    path = joinpath(PACKAGE_FOLDER,"abm_output","$input_fname.dat")
    output_no_app, output = deserialize(path)
    var_ranges = AxisKeys.axiskeys(output)
    vars = (L"\eta",L"\omega_{en}")



    mean_final_size(p) = mean(reduce(merge!,p.final_size_by_age));
    std_final_size(p) = std(reduce(merge!,p.final_size_by_age))
    base_outcome = mean_final_size(output_no_app)
    final_size_map = map(x-> (mean_final_size(x) - base_outcome),output)
    mean_weighted_degree_change(p,age) = mean(p.avg_weighted_degree_of_vaccinators[age])-mean(p.avg_weighted_degree_of_vaccinators_no_EN[age])
    weighted_degree_map = [map(p -> mean_weighted_degree_change(p,i),output) for i in 1:3]



    join_count_maps = [
        map(x-> (mean(x.assortativity_at_end)),output),
        map(x-> (mean(x.vac_join_11_counts_at_end)),output),
        map(x-> (mean(x.vac_join_01_counts_at_end)),output),
        map(x-> (mean(x.vac_join_00_counts_at_end)),output),
        # map(x-> (mean_join_00_count(x)),output),
    ]
    for en_interval in var_ranges[3]
        subfolder_path = joinpath(PACKAGE_FOLDER,"plots","app_heatmaps","$en_interval")
        mkpath(subfolder_path)
        cs = cgrad(:blues)
        datamaps = (weighted_degree_map..., final_size_map,map(std_final_size,output),join_count_maps...)
        fnames = [
            "wdg_change_Y.pdf",
            "wdg_change_M.pdf",
            "wdg_change_O.pdf", 
            "final_size_change.pdf",
            "final_size_standard_dev.pdf",
            "assortativity_preseason_count.pdf",
            "join_11_counts_preseason_count.pdf",
            "join_01_counts_preseason_count.pdf",
            "join_00_counts_preseason_count.pdf",
        ]
        titles = [
            "Change in w. deg. of vaccinators with EN",
            "Change in w. deg. of vaccinators with EN",
            "Change in w. deg. of vaccinators with EN",
            "Effect of EN on tot. infections",
            "Standard deviation from the mean of total infection size",
            "Weighted assortativity",
            "Join counts (J_11)",
            "Join counts (J_01)",
            "Join counts (J_00)",
        ]
        for (fname,title,datamap) in zip(fnames,titles,datamaps)
            p = heatmap(var_ranges[1],var_ranges[2],transpose(datamap(EN_intervals = en_interval)); title, xlabel = vars[1], ylabel = vars[2], seriescolor=cs,
             size = (800,600))
            savefig(p,joinpath(subfolder_path,"$(en_interval)_$fname"))
        end
    end
end 