


using Printf

const ts_colors = cgrad(:PuBu_9)
#plot for each day some metric of repeated notifications
function plot_model(varname,univariate_series, output_list::Vector{T},p) where T
    @unpack sim_length, immunization_intervals,infection_introduction_day = p 
    immunization_begin = first(minimum(immunization_intervals))
    x_axis = collect(1:sim_length)
    zoom((xs,ys,label,xlabel)) = (xs[infection_introduction_day:end], ys[infection_introduction_day:end],label,xlabel)
    rolling_avg(l) = [reduce(merge!,l[i:min(i+7,end)]) for i in 1:length(l)]
    ts_list(data) = [
        (x_axis, data.recorded_status_totals.S, "Susceptible over time", "no. individuals"),
        (x_axis, data.recorded_status_totals.R, "Recovered over time", "no. individuals"),
        (x_axis, data.total_vaccinators,  "No. vaccinators over time", "no. individuals"),
        (x_axis, data.mean_time_since_last_notification, "Mean time since last notification", "time (days)"),
        (x_axis, data.daily_cases_by_age.Y,"Daily Y cases", "no. individuals"),
        (x_axis, data.daily_cases_by_age.M,"Daily M cases", "no. individuals"),
        (x_axis, data.daily_cases_by_age.O,"Daily O cases", "no. individuals"),
        (x_axis, data.recorded_status_totals.I,"Total active cases", "no. individuals"),
        (x_axis, data.daily_immunized_by_age.Y, "new Y vaccinations each day", "no. individuals"),
        (x_axis, data.daily_immunized_by_age.M, "new M vaccinations each day", "no. individuals"),
        (x_axis, data.daily_immunized_by_age.O, "new O vaccinations each day", "no. individuals"),
        (x_axis, data.daily_total_notifications, "total notifications each day", "no. notifications"),
        (x_axis, data.daily_total_notified_agents, "total notified agents each day", "no. individuals"),
        (x_axis, data.total_app_weight, "total EN behaviour weight", "Total Payoff"),
        (x_axis, rolling_avg(data.avg_weighted_degree_of_vaccinator_belief), "average weighted degree of V agents", "Avg. Weighted Degree"),
        zoom((x_axis, data.daily_cases_by_age.Y, "Daily Y cases (zoomed in)", "no. individuals")),
        zoom((x_axis, data.daily_cases_by_age.M, "Daily M cases (zoomed in)", "no. individuals")),
        zoom((x_axis, data.daily_cases_by_age.O, "Daily O cases (zoomed in)", "no. individuals")),
        zoom((x_axis, data.daily_immunized_by_age.Y, "new Y vaccinations each day (zoomed in)", "no. individuals")),
        zoom((x_axis, data.daily_immunized_by_age.M, "new M vaccinations each day (zoomed in)", "no. individuals")),
        zoom((x_axis, data.daily_immunized_by_age.O, "new O vaccinations each day (zoomed in)", "no. individuals")),
        # (x_axis, data.vac_join_counts, "join counts", "no. joins"),
        # (x_axis, data.vac_join_01_counts, "join 01 counts", "no. joins"),
    ]
    sample_data = ts_list(output_list[1])
    l = length(sample_data)
    plts = [plot() for i in 1:l]
    for (i,(p,data)) in enumerate(zip(univariate_series, output_list))
        # display(p[varname])
        if !isnothing(varname) 
            p_val = typeof(p[varname])<:Real ?
                (@sprintf "%.4f" (p[varname])) :
                string(p[varname]) 
            
            labelname = "$varname = $(p_val)"
        else
            labelname = nothing
        end
        for (plt,(xs,ts,title,ylabel)) in zip(plts,ts_list(data))
            p = plot!(plt,xs,mean.(ts); #yerr = std.(ts),
                    label = labelname, line_z = i, color=:blues,
                    ylabel = ylabel,
                    colorbar = false,
                    title = title,
                    legends = :outerright,
                    xlabel = "time (days)",
            ) 
        end
    end
    # plot!(plts[begin]; )
    for (p,data) in zip(plts,sample_data)
        first_day = minimum(data[1])
        if infection_introduction_day>first_day
            vline!(p,[infection_introduction_day]; label = "infection begin", line =:dot)
        end
        for I in immunization_intervals 
            if last(I)>first_day
                vspan!(p,[max(first(I),first_day),last(I)]; label = "vaccination", color = :cyan, alpha = 0.1)    
            end
        end
    end
    return plts
end
