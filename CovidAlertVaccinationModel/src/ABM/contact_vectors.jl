#do this only at the start

"""
Called in the inner loop of `generate_contact_vectors!`, this function removes the degree corresponding to kth entry of index_lists, replaces it with another randomly sampled degree, and returns `csum` adjusted to the new samples. 

We repeat this function until `csum == 0`. 
"""
@inline function reindex!(k,csum,index_list_i,index_list_j,j_to_i_contacts,i_to_j_contacts,sample_list_i,sample_list_j)
    i_index = index_list_i[k]
    j_index = index_list_j[k]    
    csum +=  j_to_i_contacts[j_index] - i_to_j_contacts[i_index] +  sample_list_i[k] -  sample_list_j[k]
    i_to_j_contacts[i_index] = sample_list_i[k] 
    j_to_i_contacts[j_index] = sample_list_j[k]    
    return csum
end
@inline function reindex!(k,csum,index_list_i,i_to_i_contacts,sample_list_i)
    i_index = index_list_i[k]  
    csum +=  i_to_i_contacts[i_index] -  sample_list_i[k]
    i_to_i_contacts[i_index] = sample_list_i[k]   
    return csum
end

"""
    generate_contact_vectors!(ij_dist<:Distribution,ji_dist<:Distribution,i_to_j_contacts::Vector{T}, j_to_i_contacts::Vector{T})

Fills i_to_j_contacts and j_to_i_contacts with degrees sampled from ij_dist and ji_dist, such that i_to_j_contacts and j_to_i_contacts have equal sum.

Given `μz_i = mean(ij_dist)` and `μ_j = mean(ji_dist)`, these must satisfy `μ_i* length(i_to_j_contacts) == μ_j* length(j_to_i_contacts)`  
"""
function generate_contact_vectors!(ij_dist,ji_dist,i_to_j_contacts::AbstractVector{T}, j_to_i_contacts::AbstractVector{T}) where T
    
    rand!(Random.default_rng(Threads.threadid()),ij_dist,i_to_j_contacts)
    rand!(Random.default_rng(Threads.threadid()),ji_dist,j_to_i_contacts)
    l_i = length(i_to_j_contacts)
    l_j = length(j_to_i_contacts)

    csum = sum(i_to_j_contacts) - sum(j_to_i_contacts)

    inner_iter = 10
    index_list_i = MArray{Tuple{10,},Int,1,10}(undef)
    index_list_j = MArray{Tuple{10,},Int,1,10}(undef)
    sample_list_i = MArray{Tuple{10,},Int,1,10}(undef)
    sample_list_j = MArray{Tuple{10,},Int,1,10}(undef)
    
    while csum != 0
        sample!(Random.default_rng(Threads.threadid()),1:l_i,index_list_i)
        sample!(Random.default_rng(Threads.threadid()),1:l_j,index_list_j)
        rand!(Random.default_rng(Threads.threadid()),ij_dist,sample_list_i)
        rand!(Random.default_rng(Threads.threadid()),ji_dist,sample_list_j)
        @inbounds for i = 1:inner_iter
            if csum != 0
                csum = reindex!(i,csum,index_list_i,index_list_j,j_to_i_contacts,i_to_j_contacts,sample_list_i,sample_list_j)
            end
        end
    end
    return nothing
end
function generate_contact_vectors!(ii_dist,i_to_i_contacts::Vector{T}) where T
    
    rand!(Random.default_rng(Threads.threadid()),ii_dist,i_to_i_contacts)
    l_i = length(i_to_i_contacts)

    csum = sum(i_to_i_contacts)
    inner_iter = 10
    index_list_i = @MVector zeros(Int,inner_iter)
    sample_list_i = @MVector zeros(T,inner_iter)
    
    while csum % 2 != 0
        sample!(Random.default_rng(Threads.threadid()),1:l_i,index_list_i)
        rand!(Random.default_rng(Threads.threadid()),ii_dist,sample_list_i)
        @inbounds for i = 1:inner_iter
            if csum != 0
                csum = reindex!(i,csum,index_list_i,i_to_i_contacts,sample_list_i)
            end
        end
    end
    return nothing
end