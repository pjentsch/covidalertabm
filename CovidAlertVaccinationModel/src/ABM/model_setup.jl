 function get_parameters()#(0.0000,0.00048,0.0005,0.16,-1.30,-1.24,-0.8,0.35,0.35,0.35,0.2)
    
    sim_length = 500
    infection_introduction_day = 180

    params = (
        sim_length = sim_length,
        num_households = 5000,
        I_0_fraction = 0.003,
        β_y = 0.00141,
        β_m = 0.00099,
        β_o = 0.29,
        α_y = 0.4,
        α_m = 0.4,
        α_o = 0.4,
        recovery_rate = 1/5,
        π_base_y = -1.11,
        π_base_m = -1.16,
        π_base_o = -0.71,
        η = 0.5,
        κ = 0.0,
        ω = 0.0061,
        ω_en = 0.05,
        Γ = 0.906,
        ξ = 5.0,
        notification_parameter = 0.0005,
        vaccinator_prob = 0.6,
        app_user_fraction = 0.0,
        notification_threshold = 2,
        immunizing = true,
        immunization_delay = 14,
        immunization_intervals = [Intervals.Interval(60,81), Intervals.Interval(infection_introduction_day, sim_length)], 
        infection_introduction_day = infection_introduction_day,
        EN_intervals = Intervals.Interval(0.0,sim_length) ,
        ζ = 1.7
    )
    return params
end
function get_app_parameters()
    return merge(get_parameters(),(app_user_fraction = 0.3,))
end
function get_u_0(nodes,vaccinator_prob)
    is_vaccinator = rand(Random.default_rng(Threads.threadid()),nodes) .< vaccinator_prob
    status = fill(Susceptible,nodes)
    return status,is_vaccinator
end

function app_users(demographics,app_usage_prob)
    ymo_usage = [
        0.979,
        0.921,
        0.604,  
    ]
    is_app_user = Vector{Bool}(undef,length(demographics))
    @inbounds for i in eachindex(demographics)
        demo = demographics[i]
        is_app_user[i] = rand(Random.default_rng(Threads.threadid())) < app_usage_prob*ymo_usage[Int(demo)]
    end
    return is_app_user
end


mutable struct ModelSolution{T,InfNet,SocNet,WSMixingDist,RestMixingDist,RecorderType}
    sim_length::Int
    nodes::Int
    params::T
    u_next_inf::Vector{AgentStatus}
    u_next_vac::Vector{Bool}
    u_inf::Vector{AgentStatus}
    u_vac::Vector{Bool}
    covid_alert_notifications::Array{Bool,2}
    time_of_last_alert::Vector{Int}
    inf_network::InfNet
    soc_network::SocNet
    index_vectors::Vector{Vector{Int}}
    demographics::Vector{AgentDemographic}
    is_app_user::Vector{Bool}
    ws_matrix_tuple::WSMixingDist
    rest_matrix_tuple::RestMixingDist
    immunization_countdown::Vector{Int}
    output_data::RecorderType
    @views function ModelSolution(sim_length,params::T,num_households; record_degrees = false) where T
        demographics,base_network,index_vectors = generate_population(num_households)
        nodes = length(demographics)
        pop_sizes = length.(index_vectors)
        ws_mixing_tuple_preshift = deepcopy(workschool_mixing)
        rest_mixing_tuple_preshift = deepcopy(rest_mixing)

        map_symmetrize(m_tuple) = map(md -> symmetrize_means(pop_sizes,md), m_tuple)

        ws_matrix_tuple = map_symmetrize(ws_mixing_tuple_preshift) 
        rest_matrix_tuple = map_symmetrize(rest_mixing_tuple_preshift)
        # display(mean.(rest_matrix_tuple.daily))

        is_app_user = app_users(demographics,params.app_user_fraction)

        u_0_inf,u_0_vac = get_u_0(nodes,params.vaccinator_prob)

        infected_mixing_graph,soc_mixing_graph = time_dep_mixing_graphs(sim_length,base_network,demographics,index_vectors,ws_matrix_tuple,rest_matrix_tuple)
      
        covid_alert_notifications = zeros(Bool,14,nodes) #two weeks worth of values
        time_of_last_alert = fill(-1,nodes) #time of last alert is negative if no alert has been recieved

        immunization_countdown = fill(-1, nodes) #immunization countdown is negative if not counting down
        output_data = Recorder(0.0,sim_length+1; record_degrees)
        output_data.recorded_status_totals[:,1] = [count(==(AgentStatus(i)), u_0_inf) for i in 1:4]
        
        return new{T,typeof(infected_mixing_graph),
        typeof(soc_mixing_graph),typeof(ws_matrix_tuple),
        typeof(rest_matrix_tuple), typeof(output_data)}(
            sim_length,
            nodes,
            params,
            u_0_inf,
            u_0_vac,
            copy(u_0_inf),
            copy(u_0_vac),
            covid_alert_notifications,
            time_of_last_alert,
            infected_mixing_graph,
            soc_mixing_graph,
            index_vectors,
            demographics,
            is_app_user,
            ws_matrix_tuple,
            rest_matrix_tuple,
            immunization_countdown,
            output_data
        )
    end
end
