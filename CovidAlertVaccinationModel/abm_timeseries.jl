using CovidAlertVaccinationModel
using OnlineStats
using Plots
pgfplotsx()
using CovidAlertVaccinationModel:vaccination_data,ymo_vac,ymo_attack_rate
const samples = 25

function solve_and_plot_parameters()
    p =  CovidAlertVaccinationModel.get_app_parameters()
    display(p)
    out,avg_populations = mean_solve(samples, p, record_degrees = true)
    p = plot_model(nothing,[nothing],[out],p)

    ymo_vaccination_ts = mean.(out.daily_immunized_by_age)

    total_postinf_vaccination = mean.(out.total_postinf_vaccination)#sum.(eachrow(ymo_vaccination_ts[:,180:end]))
    final_size = mean.(out.unvac_final_size_by_age)#sum.(eachrow(mean.(out.daily_unvac_cases_by_age)))
    total_preinf_vaccination = mean.(out.total_preinf_vaccination)#sum.(eachrow(ymo_vaccination_ts[:,1:180]))
    target_final_size = ymo_attack_rate .*avg_populations
    target_preinf_vac = ymo_vac .* sum(vaccination_data[1:4]) .* avg_populations
    target_postinf_vac = ymo_vac .* sum(vaccination_data[5:end]) .*avg_populations
 
    println("obs final size: $final_size, target: $target_final_size")
    println("obs preinf vac: $total_preinf_vaccination, target: $target_preinf_vac")
    println("obs postinf vac: $total_postinf_vaccination,target: $target_postinf_vac")

    total_final_size = sum.(eachrow(mean.(out.daily_cases_by_age))) 
    println("vac + unvac cases proportion: $(total_final_size./avg_populations))")

    mean_deg = mean.(out.avg_weighted_degree_of_vaccinators)
    mean_vac_deg = mean.(out.avg_weighted_degree_of_vaccinators_no_EN)
    println("mean weighted degree of vac: $(mean_deg)")
    display(sum.(eachrow(ymo_vaccination_ts)) ./avg_populations) 

    l = length(p)
    savefig(plot(p...; layout = (l,1), size=(800,400*l),leftmargin = 12Plots.mm),"timeseries.pdf")
    return out
end


out = solve_and_plot_parameters()
