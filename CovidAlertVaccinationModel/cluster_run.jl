using Distributed, ClusterManagers
addprocs_slurm(3; exeflags="-t 2")
@everywhere using IntervalsModel

tasks = [do_hh,do_rest,do_ws]
particles = 5000
futures = []
for (i,worker) in enumerate(workers())
    push!(futures,remotecall(tasks[i],worker,particles))
end

fetch.(futures)
for i in workers()
	rmprocs(i)
end