# CovidAlertVaccinationModel

This is an agent-based model for a work-in-progress paper. It simulates an influenza outbreak on a random, time-dependent disease-behavior network. The network structure is obtained from data.

This repo contains two packages:

* CovidAlertVaccinationModel


* ZeroWeightedDistributions

The former implements the ABM, and computes approximations for the age-structured contact time distributions. The latter package, ZeroWeightedDistributions, implements a helpful type ZWDist{D} representing a random variable of distribution D with added weight at zero.

This package currently requires some of the features offered by the 1.7-beta release of julia.

This package only tested on x86-64 Linux. It should work everywhere that Julia does though. 