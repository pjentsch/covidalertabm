module ZeroWeightedDistributions
export ZWDist,rand,rand!
using Random
import Base.rand
import Random.rand!
using Distributions
import Distributions.pdf
import Distributions.mean
using StatsBase

struct ZWDist{BaseDistType <: Sampleable,T} <: Sampleable{Univariate,T}
    α::Float64
    base_dist::BaseDistType
    function ZWDist(α,base_dist::Sampleable{Univariate,S}) where S
        return new{typeof(base_dist),S}(α,base_dist)
    end
    function ZWDist(D::Type{<:Sampleable{Univariate,S}},α,p...) where {S}
        return new{D,S}(α,D(p...))
    end
    function ZWDist{DType}(α,p...) where {S,DType<:Sampleable{Univariate,S}}
        return new{DType,S}(α,DType(p...))
    end
end
StatsBase.mean(d::ZWDist{Dist,T}) where {Dist,T} = (1 - d.α)*StatsBase.mean(d.base_dist)


function Distributions.pdf(d::ZWDist, x)
    if x == 0 
        return d.α + (1-d.α)*pdf(d.base_dist,0)
    else
        return (1-d.α)*pdf(d.base_dist,x)
    end
end

function Distributions.mean(d::ZWDist, x)
    return (1-d.α)*StatsBase.mean(d.base_dist,0)
end

function Base.rand(rng::AbstractRNG, s::ZWDist{DType,S}) where {DType,S} 
    return ifelse(Base.rand(rng) < s.α, zero(eltype(DType)), Base.rand(rng,s.base_dist))
end



end
