using ZeroWeightedDistributions
using KernelDensity
using Distributions
using Test
using ThreadsX
using RandomNumbers.Xorshifts
# using Plots
const dist_list = [Poisson]
const params_list = collect(0.1:0.2:0.9)
const α_list = collect(0.2:0.2:1.0)
const RNG = Xoroshiro128Star(1)
const tol = 0.01
const sample_length = 1_000_000
epdf(samples,k) = count(==(k), samples)/sample_length

function test_dist_vec(d)
    samples_vec = rand(RNG,d, sample_length)   
    vec_err = all(abs(epdf(samples_vec,k) - pdf(d,k)) < tol for k in 0:1:10)
    return vec_err
end
function test_dist_map(d)
    samples_map = map(x->rand(RNG,d), 1:sample_length)
    map_err= all(abs(epdf(samples_map,k) - pdf(d,k)) < tol for k in 0:1:10)
    return map_err
end


@testset "ZeroWeightedDistributions.jl" begin
    
    
    @testset for(d,p,α) in Iterators.product(dist_list,params_list,α_list)
        println((d,p,α))
        @test test_dist_vec(ZWDist(d,α,p))
    end

    @testset for(d,p,α) in Iterators.product(dist_list,params_list,α_list)
        println((d,p,α))
        @test test_dist_map(ZWDist(d,α,p))
    end
end
