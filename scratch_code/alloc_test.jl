using StaticArrays
using StrideArrays
using BenchmarkTools
using UnsafeArrays

function g() #would like to reduce allocations here
    l = rand(9000:10_000) #large random integer 
    arr = Vector{Int}(undef,l) #array of length l

    #do some stuff with arr
    accum = 0
    arr .= 1
    for k in arr
        accum += k + f(arr)
    end

    return accum #discard arr
end
function f(arr)
    return arr[1]*2
end


@allocated g()